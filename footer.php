<?php
/**
 *	The template for displaying the footer.
 *
 *	Contains the closing of the #content div and all content after.
 *
 *	@link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 *	@package WordPress
 *	@subpackage rise-lite
 */
?>
<?php
$rise_lite_email_icon = get_theme_mod( 'rise_lite_email_icon', get_stylesheet_directory_uri() . '/assets/images/footer-email-icon.png' );
$zerif_email = get_theme_mod( 'zerif_email', '<a href="mailto:contact@site.com">contact@site.com</a>' );
$rise_lite_phone_icon = get_theme_mod( 'rise_lite_phone_icon', get_stylesheet_directory_uri() . '/assets/images/footer-telephone-icon.png' );
$zerif_phone = get_theme_mod( 'zerif_phone','<a href="tel:0 332 548 954">0 332 548 954</a>' );
$zerif_rise_icon_address_icon = get_theme_mod( 'zerif_rise_icon_address_icon', get_stylesheet_directory_uri() . '/assets/images/footer-address-icon.png' );
$zerif_address = get_theme_mod( 'zerif_address',__('Company address', 'rise-lite' ) );
$zerif_copyright = get_theme_mod('zerif_copyright');
$zerif_accessibility = get_theme_mod('zerif_accessibility');
$attribut_new_tab = (isset($zerif_accessibility) && ($zerif_accessibility != 1) ? ' target="_blank"' : '' );
?>
				</div><!-- .site-content -->
				<footer id="footer" role="contentinfo">
					<div class="top-footer clearfix">
						<div class="row">
							<?php if( $zerif_rise_icon_address_icon || $zerif_address ): ?>
								<div class="col-sm-4">
									<?php if( $zerif_rise_icon_address_icon ): ?>
										<img src="<?php echo esc_url( $zerif_rise_icon_address_icon ); ?>" alt="<?php _e( 'Address', 'rise-lite' ); ?>" title="<?php _e( 'Address', 'rise-lite' ); ?>" />
									<?php endif; ?>
									<?php if( $zerif_address ): ?>
										<p><?php echo esc_html( $zerif_address ); ?></p>
									<?php endif; ?>
								</div><!--/.col-sm-4-->
							<?php endif; ?>

							<?php if( $rise_lite_email_icon || $zerif_email ): ?>
								<div class="col-sm-4">
									<?php if( $rise_lite_email_icon ): ?>
										<img src="<?php echo esc_url( $rise_lite_email_icon ); ?>" alt="<?php _e( 'E-mail', 'rise-lite' ); ?>" title="<?php _e( 'E-mail', 'rise-lite' ); ?>" />
									<?php endif; ?>
									<?php if( $zerif_email ): ?>
										<p><?php echo $zerif_email; ?></p>
									<?php endif; ?>
								</div><!--/.col-sm-4-->
							<?php endif; ?>

							<?php if( $rise_lite_phone_icon || $zerif_phone ): ?>
								<div class="col-sm-4">
									<?php if( $rise_lite_phone_icon ): ?>
										<img src="<?php echo esc_url( $rise_lite_phone_icon ); ?>" alt="<?php _e( 'Telephone', 'rise-lite' ); ?>" title="<?php _e( 'Telephone', 'rise-lite' ); ?>" />
									<?php endif; ?>
									<?php if( $zerif_phone ): ?>
										<p><?php echo $zerif_phone; ?></p>
									<?php endif; ?>
								</div><!--/.col-sm-4-->
							<?php endif; ?>
						</div><!--/.row-->
					</div><!--/.top-footer.clearfix-->
					<div class="bottom-footer">
						<div class="container">
							<div class="row">
								<div class="col-sm-12">
									<p><a href="http://themeisle.com/themes/zerif-pro-one-page-wordpress-theme/?ref=6148" <?php echo $attribut_new_tab; ?> rel="nofollow">Rise Lite </a><?php echo _e( 'powered by','rise-lite' ); ?> <a href="http://wordpress.org/" <?php echo $attribut_new_tab; ?> rel="nofollow">WordPress</a></p>
								</div><!--/.col-sm-12-->
							</div><!--/.row-->
						</div><!--/.container-->
					</div><!--/.bottom-footer-->
				</footer><!--/#footer-->
			</div>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>